# Casos d'ús

Compilació de casos útils que es poden adaptar a les vostres necessitats o obtenir idees per construir els vostres propis laboratoris utilitzant IsardVDI.

A la carpeta **guests** trobareu les instal·lacions del sistema operatiu d'escriptoris virtuals per a Linux i Windows que estan optimitzades perquè siguin òptimes en un sistema de virtualització.

A la carpeta **labs** hem compilat escenaris virtuals que poden ser útils per veure les capacitats IsardVDI.