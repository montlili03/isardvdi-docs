# Queue System

Queue System is developed using [RQ](https://python-rq.org), a library that provides a job management system with a [Redis](https://redis.io) backend. Currently `isard-api` and `isard-storage` containers are using the Queue System. So, these containers should have access to TCP port 6379 of `isard-redis` container.

## Queues

### api

Consumed by `isard-api`

Tasks:

- `feedback`: Send feedback to user via socketio about job status.

### storage.&lt;storage_pool_id&gt;.high

Consumed by isard-storage that have this `storage_pool_id` configured with **best priority**

Tasks:

- `upload`: Create a unique URL to upload media or disks.
- `create_disk`: Create a disk.

### storage.&lt;storage_pool_id&gt;.default

Consumed by isard-storage that have this `storage_pool_id` configured with **medium priority**


Tasks:

- `create_snapshot`: Create snapshot via `qemu-img`.
- `download`: Create a downoadable item and create a unique URL to upload media or disks.
- `unarchive_disk`: Recover an archived disk decompressing it and moving it to high performance storage.
- `resize`: Resize disk via `qemu-img`.

### storage.&lt;storage_pool_id&gt;.low


Consumed by isard-storage that have this `storage_pool_id` configured with **poor priority**

Tasks:

- `convert`: Convert disks via `qemu-img`.
- `commit`: Commit disks via `qemu-img`.
- `rebase`: Rebase disk via `qemu-img`.
- `sparsify`: Sparsify disk via `virt-sparsify`.
- `archive_disk`: Compress disk and move to low cost storage.
- `delete_disk`: Delete disk safely.
- `delete_media`: Delete media.

### hypervisor.&lt;storage_pool_id&gt;.high

Consumed by isard-hypervisor that have this `storage_pool_id` configured with **best priority**

Tasks:

### hypervisor.&lt;storage_pool_id&gt;.default

Consumed by isard-hypervisor that have this `storage_pool_id` configured with **medium priority**

Tasks:

- `check_domain`: Start a domain paused.
- `start_domain`: Start a domain.

### hypervisor.&lt;storage_pool_id&gt;.low

Consumed by isard-hypervisor that have this `storage_pool_id` configured with **poor priority**

Tasks:


### hypervisor.&lt;hypervisor_id&gt;.high

Consumed by `hypervisor_id` with **best priority**

Tasks:

- `stop_domain`: Stop a domain.

### hypervisor.&lt;hypervisor_id&gt;.default

Consumed by `hypervisor_id` with **medium priority**

Tasks:

- `stop_domain`: Stop a domain.
- `revert_snapshot`: Revert snapshot via `virsh`/libvirt.

### hypervisor.&lt;hypervisor_id&gt;.low

Consumed by `hypervisor_id` with **poor priority**

Tasks:

- `create_snapshot`: Create snapshot via `virsh`/libvirt.
