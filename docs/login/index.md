# Introduction

To access the server you have to go to https://isardvdi_domain and you'll be presented with the login page.

![](./local_access.images/login.png)

Your user account could be one of this types:

- **local**: As an user you should have been provided with your *category* and user credentials.
- **OAuth2** (*google/gitlab*): You should have been provided an access code that will be required to complete registration with your personal account.
- **SAML**: Use your enterprise credentials to login.

When using **local** authentication you can avoid having to select the category each time by going directly to your category id (ask your IsardVDI manager) appended in the login URL:

- https://isardvdi_domain/login/category_id

Then the login page will select the category automatically and you don't need to enter it each time.