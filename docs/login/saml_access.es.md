# Acceso SAML

Para acceder mediante una cuenta SAML, primero de todo necesitas tener un código de registro, que te debería de dar un Manager.

![](./saml_access.es.images/login.png)

En primer lugar, tienes que hacer click al botón ![](./saml_access.images/button.png), que está en la página principal de inicio de sesión de IsardVDI

A partir de aquí, continua con el proceso de inicio de sesión. Ésta página es distinta según cada servidor SAML. Si utilizas el inicio de sesión SAML en otro servicio, seguramente ya conoces su funcionamiento.

## Inscripción por código de registro

Si es la primera vez que se accede como usuario aparecerá el formulario para introducir un código de registro. 

El código de registro se crea por parte del manager de la organización y se explica cómo se genera más adelante en la sección de "Manager" de la documentación. Cada usuario ha de pertenecer a un grupo y tener un rol, ya sea advanced (profesor), user (alumno), etc. Por cada grupo de usuarios se pueden generar códigos de autorregistro para diferentes roles.

Se introduce el código proporcionado por un Manager y se pulsa el botón ![](./oauth_access.es.images/codigo2.png)

![](./oauth_access.es.images/codigo1.png)

Y se accede a la interfaz básica autenticados con la cuenta de SAML.

![](./oauth_access.es.images/home1.png)

Una vez autenticados, ya no hace falta repetir el proceso de registro. Cuando se quiera volver a acceder mediante SAML, sólo se tiene que pulsar en el botón ![](./saml_access.images/button.png) y utilizar las credenciales de SAML.