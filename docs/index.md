# Introduction

We've organized the documentation with:

- [**User Guide**](user/) where you'll find how to use IsardVDI to create desktops, templates, deployments, ...
- [**Administration Guide**](admin/)  for **manager** and **administrator** roles with management advanced administration interface to manage everything. Managers will only be able to manage withing his category while administrators will see everything and also system items like downloads or hypervisors.
- [**Install Guide**](install/requirements) has technical details to install and setup a complete IsardVDI virtualization platform.
- [**API**](api/) is being redone but now has some tips showing how to do everything through the api.
- In [**Community** ](community/info/) you'll find links to keep in contact and contribute to the project.
- [**Use Cases**](use_cases/) are helpful to get ideas you can adapt to your use case.

# IsardVDI

Virtual Desktop Infrastructure (VDI) is a technology that allows users to access virtual desktops that are hosted on a remote server or cloud infrastructure. VDI is becoming increasingly popular in organizations as it provides several benefits over traditional desktop environments, including improved security, easier management, and greater flexibility for remote work.

IsardVDI is an **open-source** VDI solution that provides a complete virtual desktop infrastructure management platform. It enables administrators to create, configure, and manage virtual desktops and applications for users across multiple devices, regardless of their location. IsardVDI uses Docker containers to provide a lightweight and scalable virtualization platform, which makes it highly efficient and easy to deploy.

<iframe width="800" height="400" src="https://www.youtube.com/embed/modrGJ_Szyk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Feel free to try it from [IsardVDI Gitlab](https://gitlab.com/isard/isardvdi) or open us a ticket with your request at [IsardVDI Gitlab issues](https://gitlab.com/isard/isardvdi/-/issues/new).