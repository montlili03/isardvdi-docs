# Full de ruta

L'equip d'IsardVDI és un grup de desenvolupadors i administradors de sistemes dedicats a proporcionar una solució d'infraestructura d'escriptori virtual potent i flexible. Estem treballant constantment per millorar el programari i afegir noves característiques basades en els comentaris dels usuaris i les tecnologies emergents.

Aquestes són algunes de les característiques que vénen aviat:

1. **Xarxes personalitzades amb OpenVSwitch**: IsardVDI està desenvolupant un nou contenidor *isardvdi-ovs* que permetrà portar OpenVSwitch a IsardVDI a un altre nivell. Amb l'objectiu de donar autonomia als usuaris, aquesta funció permetrà crear xarxes pròpies configurades entre escriptoris, amb serveis opcionals com DHCP o connexió de xarxa amb NAT.

Podeu seguir el seu desenvolupament principalment a [https://gitlab.com/isard/isardvdi/-/merge_requests/1045](https://gitlab.com/isard/isardvdi/-/merge_requests/1045)

2. **Unitats personals**: Aquesta característica connectarà automàticament les unitats d'emmagatzematge de xarxa des d'una instància del Nextcloud als escriptoris virtuals de l'usuari. També permet accedir a aquest emmagatzematge fora dels escriptoris virtuals, des de la interfície web d'IsardVDI o fins i tot amb l'aplicació client nextcloud en qualsevol dispositiu. Amb aquesta característica, la característica *escriptoris temporals* tindrà més sentit, ja que l'usuari tindrà la seva pròpia unitat de xarxa personal connectada als seus escriptoris virtuals com a emmagatzematge persistent, permetent eliminar l'escriptori virtual en aturar-se, reduint així l'emmagatzematge utilitzat.

Podeu seguir el seu desenvolupament a [https://gitlab.com/isard/isardvdi/-/merge_requests/1259](https://gitlab.com/isard/isardvdi/-/merge_requests/1259)

3. **Sistema de cua d'esdeveniments**: Hi ha moltes característiques que venen amb el sistema de cua d'esdeveniments. Això permetrà tenir contenidors *isard-storage* que consumiran les accions afegides per l'usuari o el programador. Aquestes accions poden trigar molt de temps i es gestionaran basant-se en la prioritat de les cues i la càrrega del consumidor. Per exemple, fent una sparsify de disc, instantània, convert, pujada, baixada, ...

Podeu seguir el seu desenvolupament a [https://gitlab.com/isard/isardvdi/-/merge_requests/1815](https://gitlab.com/isard/isardvdi/-/merge_requests/1815)

4. **Migració en temps real**: Quan escaleu l'IsardVDI a múltiples hipervisors en infraestructura, voleu reduir els servidors en línia segons la demanda. Per permetre temps més petits d'aturada de l'hipervisor, els escriptoris virtuals que s'utilitzen que estan iniciats en un hipervisor es migraran en directe a un altre, sense que gairebé l'usuari ho noti.

5. **Bina de reciclatge**: Els escriptoris i les plantilles no s'eliminaran immediatament i es mantindran per a un interval seleccionat en el sistema que permetrà la recuperació abans de ser eliminades de manera definida.

S'estan desenvolupant i provant moltes altres esmenes d'errors, millores i funcionalitats menors, com:

- Permetre rols avançats d'usuari per eliminar plantilles
- Accedir a l'escriptori virtual http/https des del domini principal IsardVDI
- Importar els vostres escriptoris VirtualBox com a escriptoris IsardVDI
- Exportar l'escriptori IsardVDI a KVM
- Instantànies de l'escriptori virtual
- Editar els escriptoris de desplegament
- Nou panell d'estat del sistema i administrador
- Grups d'emmagatzematge i funcions per a moure els discs entre ells
- Registres sobre les accions d'escriptori i usuaris
- Esdeveniments de correu electrònic
- ...