# Edit Desktop

You can edit the name of the desktop, its description, the viewers you want to use, leave the username and password indicated to enter with rdp, change the hardware configuration, add a GPU if you have it, add media and change the desktop image .

To be able to edit a desktop, press the icon ![](./edit_desktop.es.images/edit_desktop1.png)

![](./edit_desktop.images/edit_desktop1.png)

And the icon ![](./edit_desktop.es.images/edit_desktop3.png)

![](./edit_desktop.images/edit_desktop2.png)

And redirects to a page where you can edit.


## Viewers

In this section you can select which [viewers](/user/viewers/) you want to use to display the desktop. You can also select whether you want to start the desktop in full screen or not.

![](./edit_desktop.images/edit_desktop3.png)


## RDP Login

In this section, you can add the login user and password for the RDP viewer. This configuration is only necessary if you don't want to be authenticating yourself to the desktop by RDP all the time and if the RDP viewer is selected.

![](./edit_desktop.images/edit_desktop4.png)


## Hardware

In this section you can edit the hardware you want to have on the desktop.

* To change the boot mode of a desktop, you can change it in the "Boot" section. If an iso is added in the "Media" section, in "Boot" you should select "CD/DVD", otherwise leave it in "Hard Disk".

* Networks: Here you can select the network you want to use, by default use "Default" and if you want to use the RDP viewer, you must also select the "Wireguard VPN" network.

![](./edit_desktop.images/edit_desktop5.png)


## Reservables

In this section you can select the GPU card that you want to associate with the desktop if you have one available.

![](./edit_desktop.images/edit_desktop6.png)


## Media

In this section you can add an iso that has been shared with the user.

![](./edit_desktop.images/edit_desktop7.png)



## Image

In this section you can select the cover image that the desktop will have in the main view.

![](./edit_desktop.images/edit_desktop8.png)