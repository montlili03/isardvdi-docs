# Xarxes

Els escriptoris de l'IsardVDI poden tenir diverses targetes de xarxa alhora i poden ser de diferents tipus:

- **predeterminat**: Proporciona una adreça dinàmica (DHCP) amb DNS i passarel·la a través d'un encaminador NAT. Té connexió a Internet i està aïllat d'altres escriptoris del sistema.
- **personal**: El tipus de xarxa personal adjuntat als escriptoris d'usuari estarà a la mateixa xarxa (VLAN). Amb xarxes personals, els usuaris poden configurar un entorn de xarxa privat per a les seves màquines virtuals, on poden assignar les seves pròpies adreces IP, configurar la configuració de la xarxa i controlar l'accés a la xarxa. Això pot ser útil per a escenaris com proves o entorns de desenvolupament, on múltiples escriptoris virtuals d'usuari necessiten interactuar entre ells en un entorn tancat.
- **ovs**: Les xarxes OpenVSwitch permeten connectar a la mateixa xarxa (VLAN) tots els escriptoris virtuals que l'hagin connectat, independentment del propietari de l'escriptori virtual. També es poden connectar a la mateixa xarxa VLAN en infraestructura, permetent així connectar els ordinadors físics a escriptoris virtuals. *Aquesta opció s'ha de configurar a nivell de servidor. Llegiu isardvdi.cfg per a les opcions de configuració*.
- **wireguard**: Aquesta és una xarxa de sistema que assigna adreces a l'escriptori virtual i és necessària per a connectar-se a l'escriptori des de casa i permetre connexions de visualització de tipus RDP.

NOTA: S'han d'afegir xarxes especials i establir els permisos per un administrador a [interfícies](/admin/domains/#interfaces)

Exemples:

- [**Client - Entorn del servidor**](/use_cases/client_server/client_server.ca/): Aquest laboratori mostra com crear una xarxa **ovs** i utilitzar-la entre el professor (paper avançat) i els seus estudiants (paper d'usuari).
- [**Proves de xarxa**](/use_cases/network_tests/network_tests.ca/): En aquest laboratori s'introduiran diferents xarxes IsardVDI i com configurar-les als escriptoris virtuals Linux.
- [**Xarxes personals**](/use_cases/private_and_personal_networks/private_and_personal_networks.ca/): Aquest laboratori mostra com utilitzar les xarxes privades i les xarxes personals de l'usuari.