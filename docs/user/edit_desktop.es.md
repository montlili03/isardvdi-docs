# Editar Escritorio

Se puede editar el nombre del escritorio, su descripción, los visores que se quieran utilizar, dejar indicado el usuario y contraseña para entrar con rdp, cambiarle la configuración del hardware, añadirle una GPU si tiene, añadir un media y cambiar la imagen del escritorio.

Para poder editar un escritorio se pulsa el icono ![](./edit_desktop.es.images/edit_desktop1.png)

![](./edit_desktop.es.images/edit_desktop2.png)

Y el icono ![](./edit_desktop.es.images/edit_desktop3.png)

![](./edit_desktop.es.images/edit_desktop4.png)

Y redirecciona a una página donde poder editar.


## Visores

En este apartado se puede seleccionar qué [visores](/user/viewers.es/) se quieren utilizar para visualizar el escritorio. También se puede seleccionar si se quiere arrancar el escritorio en pantalla completa o no.

![](./edit_desktop.es.images/edit_desktop5.png)


## Login RDP

En este apartado se añade el usuario y contraseña de login para el visor de RDP. Ésta configuración sólo es necesaria si no se quiere estar identificándose en el escritorio por RDP todo el rato y si el visor RDP está seleccionado.

![](./edit_desktop.es.images/edit_desktop6.png)


## Hardware

En este apartado se puede editar el hardware que se quiera tener en el escritorio.

* Para cambiar el modo de arranque de un escritorio, se cambia en la sección de "Boot". Si en la sección de "Media" se añade una iso, en "Boot" se debería seleccionar "CD/DVD", sino se deja en "Hard Disk".

* Redes: Aquí se puede seleccionar la red que se quiera utilizar, por defecto se utiliza "Default" y si se quiere utilizar el visor RDP, se tiene que seleccionar además la red de "Wireguard VPN".

![](./edit_desktop.es.images/edit_desktop7.png)


## Reservables

En este apartado se puede seleccionar la tarjeta de GPU que se quiera asociar al escritorio si es que tiene alguna disponible.

![](./edit_desktop.es.images/edit_desktop8.png)


## Media

En este apartado se le puede añadir un iso que esté compartida con el usuario.

![](./edit_desktop.es.images/edit_desktop9.png)



## Imagen

En este apartado se puede seleccionar la imagen de portada que tendrá el escritorio en la vista principal.

![](./edit_desktop.es.images/edit_desktop10.png)