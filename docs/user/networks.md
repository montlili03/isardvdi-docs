# Networks

Desktops in IsardVDI can have multiple network cards at the same time and can be of different kinds:

- **default**: Provides dynamic address asignment (DHCP) with DNS and Gateway through a NAT router. It has Internet connection and it is isolated from other desktops in system.
- **personal**: Personal networks kind attached to user desktops will be in the same network (VLAN). With personal networks, users can set up a private network environment for their virtual machines, where they can assign their own IP addresses, configure network settings, and control access to the network. This can be useful for scenarios such as testing or development environments, where multiple user virtual desktops need to interact with each other in a closed environment.
- **ovs**: OpenVSwitch networks allows to connect in the same network (VLAN) all the virtual desktops that have it attached, regardless of the virtual desktop owner. They can also be connected to the same VLAN network in infrastructure, thus allowin to connect phisical hosts to virtual desktops. *This option has to be setup at server level. Read isardvdi.cfg for setup options*.
- **wireguard**: This is a system network that assigns addressing to the virtual desktop and it is needed for connecting to the desktop from home and to allow RDP type viewer connections.

NOTE: Special networks should be added and permissions set by an administrator at [interfaces](/admin/domains/#interfaces)

Examples:

- [**Client - Server environment**](/use_cases/client_server/client_server/): This lab shows how to create an **ovs** network and use it between teacher (advanced role) and his students (user roles).
- [**Network tests**](/use_cases/network_tests/network_tests/): In this lab will introduce different IsardVDI networks and how to setup in Linux virtual desktops.
- [**Personal networks**](/use_cases/private_and_personal_networks/private_and_personal_networks/): This lab shows how to use the private networks and de user personal networks.


