# Linkat 22.04 LTS install

# Instal·lació del SO

Per a transformar-la en la Linkat s'ha executat les [instruccions oficials](http://linkat.xtec.cat/portal_linkat/wikilinkat/index.php/Linkat_edu_22.04_Autonom). En un escriptori amb Ubuntu 22.04 LTS instal·lat:

```
$ wget http://download-linkat.xtec.cat/distribution/linkat-edu-22.04/main/linkat-install_22.04-3_all.deb
$ sudo dpkg -i linkat-install_22.04-3_all.deb
```

1. S'obre l'aplicació instal·ladora de Linkat. 

2. S'obrirà un terminal i demanarà la clau d'administrador.

3. Es selecciona el gestor de finestres **lightdm** (Mate).

4. Es reinicia l'equip en cabar.


## Isard (rols admin o manager)

**Modificar XML de l'escriptori**

![](../../ubuntu_22.04/desktop/installation/images/modify_xml.png){width="40%"}

```
<driver name="qemu" type="qcow2" cache="unsafe" discard="unmap"/>
```

## Configuració

**Estat dels serveis**

- apparmor: actiu
- tallafocs ufw: actiu
- sshd: no instal·lat


### Terminal

**Comandaments bàsic**
```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim gedit vlc net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```

**Deshabilitar actualitzacions automàtiques i les notificacions de les noves**
```
$ sudo apt purge update-manager update-notifier*
$ sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::DevRelease "false";
$ sudo vim /etc/update-manager/release-upgrades
Prompt=never
$ sudo snap refresh --hold 
$ sudo snap set system refresh.retain=3
```

**Habilitar l'auto-login de l'usuari**
```
$ sudo vim /usr/share/lightdm/lightdm.conf.d/50-arctica-greeter.conf
# add next line to the end of file
autologin-user=isard
```

**Habilitar l'accés via RDP**
```
$ sudo apt install xrdp
$ sudo systemctl enable xrdp
$ sudo systemctl restart xrdp

$ sudo vim /etc/X11/Xsession.d/80mate-environment
# add next line before 'fi' closes, last line inside conditional
unset DBUS_SESSION_BUS_ADDRESS
```

**Redimensionat de pantalla**

```
$ sudo echo 'ACTION=="change",KERNEL=="card0", SUBSYSTEM=="drm", RUN+="/usr/local/bin/x-resize"' > /etc/udev/rules.d/50-x-resize.rules
$ sudo vim /usr/local/bin/x-resize
#! /bin/sh
PATH=/usr/bin
export DISPLAY=:0.0
xrandr --output "$(xrandr | awk '/ connected/{print $1; exit; }')" --auto

$ sudo chmod 744 /usr/local/bin/x-resize
```

**Eliminar imatges de kernel sense ús**
```
$ sudo dpkg --get-selections | grep linux-image
$ sudo uname -a
$ sudo apt-get purge linux-image-5.19.0-32-generic
$ sudo apt-get purge linux-image-generic-hwe-22.04
```

### Paràmetres del sistema

**Administració - Programari i actualitzacions**

![](../ubuntu_22.04/desktop/installation/images/mfglH6f.png){width="70%"}

**Maquinari - Gestor d'energia**

![](../ubuntu_22.04/desktop/installation/images/LM9q2Yz.png){width="70%"}

**Aspecte i comportament - Estalvi de pantalla**

![](../ubuntu_22.04/desktop/installation/images/zH0YWLI.png){width="70%"}


## Personalització

### Terminal

**Canviar el fons de pantalla del login**
```
$ sudo vim /usr/share/glib-2.0/schemas/30_ubuntu-mate.gschema.override
background='/path/to/wallpaper'
$ sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
```


### Paràmetres del sistema

**Personal - Quant a mi**

![](../ubuntu_22.04/desktop/installation/images/eAwSqAf.png){width="70%"}