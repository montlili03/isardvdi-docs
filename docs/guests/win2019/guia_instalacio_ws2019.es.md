# Guia de instalación Windows *Server 2019

La intención de esta guia es mostrar paso a paso como crear una imagen base de Windows *Server 2019 para crear una plantilla adaptada en el entorno de virtualización de escritorios de *IsardVDI.

La idea de este documento es aportar y recoger ideas sobre qué elementos de Windows modificar y personalizar por un mejor rendimiento.


## *Pre-instalación Windows *Server 2019

### Creación del escritorio

1.- Crear **nuevo escritorio** desde el panel principal

2.- Aplicar **visores** *sel·leccionats, redes ***Default** y **VPN** y marcar el **arranque del escritorio desde imagen ISO**

![](./guia_instalacio_ws2019.images/dWNL9cl.png)


3.- Crear el escritorio en base a la plantilla **Windows *Server 2019 ISO *install**

![*create_*desktop_*install](./guia_instalacio_ws2019.images/uJIyLj9.png)


4.- En el apartado **Media** del escritorio se añadirán las imágenes que lleva la plantilla, como son el **instalador de Windows *Server 2019** y los **controladores de *VirtIO** con el agente de instalación("*win2019 *eval" y "*virtio-*win" respectivamente)

![](./guia_instalacio_ws2019.images/EHwqTEc.png)
 

5.- Una vez creada, **arrancar el escritorio**


## Instalación

### Sistema Operativo

1.- Si sale esta pantalla al arranque, **enviar "*Ctr+Alto+*Supr"**

![](./guia_instalacio_ws2019.images/8oqNxy8.png)

![](./guia_instalacio_ws2019.images/JG4kD1Q.png)

![](./guia_instalacio_ws2019.images/dHk0hkw.png)


2.- Escoger **"*Experiencia de *escritorio"** por **"*Standard *Evaluation"**

![](./guia_instalacio_ws2019.images/OVUbe6T.png)


3.- Escoger la **instalación personalizada**

![](./guia_instalacio_ws2019.images/h4LGdaT.png)


### Cargar *drivers


1.- Ahora se tienen que **cargar los controladores de *VirtIO** porque el sistema operativo lo reconozca como un **tipo válido de disco**. Se hace clic a "*Cargar controlador"

![](./guia_instalacio_ws2019.images/CZm4guM.png)


2.- Se busca al explorador la **unidad de disco *extraible** correspondiente a la imagen ***virtio-*win** y se *sel·lecionna el archivo correspondiente para Windows *Server 2019

![](./guia_instalacio_ws2019.images/VvDyAED.png)

![](./guia_instalacio_ws2019.images/bm5lrRc.png)

![](./guia_instalacio_ws2019.images/0Nyv2cG.png)

![](./guia_instalacio_ws2019.images/qwgRZDo.png)


3.- **Mostrar** los detalles del error de conexión y **aceptar**

![](./guia_instalacio_ws2019.images/9I0BEIi.png)

![](./guia_instalacio_ws2019.images/iSCDywB.png)


4.- Hacer clic a **"*Siguiente"** y **empezará la instalación del sistema operativo**

![](./guia_instalacio_ws2019.images/oFiNaaf.png)


## Puesto-instalación

### Modificación del escritorio

1.- **Cambiar el arranque** del escritorio desde ISO a arrancar con el **disco duro**

![](./guia_instalacio_ws2019.images/dpBb8Pw.png)

![](./guia_instalacio_ws2019.images/acLq1Ng.png)


2.- Se arranca el escritorio


3.- El primero que se abrirá será este panel donde se tiene que cambiar el password. En este caso a ****Aneto_3404***

![](./guia_instalacio_ws2019.images/YPy7q5M.png)


4.- Se **envia** otra vez la combinación **"*Ctrl+Alto+*Supr"**

![](./guia_instalacio_ws2019.images/beVwtF5.png)


### Instalación de *drivers

1.- Se abre otra vez la imagen de **"*virtio-*win"** y se ejecuta el archivo para **instalar el agente** y los controladores necesarios de *VirtIO, como por ejemplo, los componentes de red

![](./guia_instalacio_ws2019.images/ipRyw14.png)

![](./guia_instalacio_ws2019.images/Kt5h8a1.png)

![](./guia_instalacio_ws2019.images/9Fhlfg8.png)

![](./guia_instalacio_ws2019.images/jwPcjg4.png)

![](./guia_instalacio_ws2019.images/8dPDa6i.png)

![](./guia_instalacio_ws2019.images/e7PGTOF.png)


2.- Se **reinicia el equipo** y se instalan la **'*virtio-*win-*guest-*tools'**, donde hay el **'*qemu-*guest-agente'**, que permitirá la comunicación del sistema del escritorio con el **visor**, y asi pueda reconocer la **redimensión de pantalla** o la lectura de **dispositivos USB** desde la máquina *hueste*

![](./guia_instalacio_ws2019.images/S9tH3vy.png)

![](./guia_instalacio_ws2019.images/VPJ1kaY.png)


### Escritorio remoto

1.- Se habilita el **escritorio remoto** a la **'*Configuración'** del sistema

![](./guia_instalacio_ws2019.images/OmAPWKi.png)

![](./guia_instalacio_ws2019.images/zcExQev.png)

![](./guia_instalacio_ws2019.images/tglLC70.png)


2.- A **configuración avanzada**:

![](./guia_instalacio_ws2019.images/kK4hnmR.png)

![](./guia_instalacio_ws2019.images/YD6EGoK.png)


### Verificación de redes

Se verifican el **correcto funcionamiento** de las **redes** configuradas con el mando ```*ipconfig``` abriendo un *CMD

![](./guia_instalacio_ws2019.images/4IPIky9.png)


### *Login por visor *RDP VPN

Se modifica el escritorio y se **activa el visor "*RDP VPN"**, y se **cambian las credenciales** por qué sean las **mismas** que el usuario y *contrassenya que el ***login del Windows**.

De esta forma se **conectará automáticamente por *RDP sin validación**.

(Siguiendo los pasos de https://learn.microsoft.com/en-us/troubleshoot/windows-server/user-profiles-and-logon/turn-on-automatic-logon)

![](./guia_instalacio_ws2019.images/957mTsW.png)


## Creación de T3

Las **plantillas T3** son las plantillas de **Windows** donde se ha **pasado el software **Optimization *Tools***. 
Este **saca** los **servicios inutilizados** que llenan **espacio** en disco y memoria y ***empeoren** la gestión de energia. 
**Mejora** asi el **rendimiento** en la totalidad el escritorio.

Los pasos del manual hasta aqui explican la **creación de la plantilla T1**, donde una vez acabada la instalación se sacan las imágenes del apartado **Media** y se convierte en plantilla T1.- 

En este último punto se ejecuta el software para **crear la T3**.


### *Optimization *tools

Se ejecuta la herramienta ***Optimization *Tools** tal y como se [explica en este apartado del manual](https://isard.gitlab.io/isardvdi-docs/install/win10_install_guide.ca/#*vmware-os-*optimization-*tool) para crear una plantilla T3, la cual a Rebeco le decimos asi después de que un escritorio pase por el software de *Optimization *tools.

1.- Existe una imagen ya creada que se tiene que **añadir a la T1** y **crear un escritorio** para poder **ejecutar el programa**, y posteriormente **crear la plantilla nombrada T3**

![](./guia_instalacio_ws2019.images/Hx8YIMy.png)


2.- **Arrancar** el escritorio y ejecutar el software

![](./guia_instalacio_ws2019.images/uV9Pgy9.png)


3.- Se abre esta ventana y hacemos clic a **'*Analyze'**

![](./guia_instalacio_ws2019.images/zTnYuQV.png)


4.- A **'*Opciones comunes'** se marcan estos **parámetros**

![](./guia_instalacio_ws2019.images/yE5LsHe.png)

![](./guia_instalacio_ws2019.images/WbZhJFc.png)


5.- Se hace clic a **'*Optimizar'** para iniciar la optimización


6.- Se **reinicia** el escritorio para comprobar el correcto funcionamiento y ver que se ha **mejorado la velocidad** del sistema


7.- Finalmente se **apaga** el escritorio, se modifica y se **saca la imagen del apartado "Media"**


8.- Sobre este escritorio se **crea una nueva plantilla T3**