# Guía de instalación Windows 10

La intención de esta guía es mostrar paso a paso como crear una imagen base de windows 10 para crear una plantilla adaptada al entorno de virtualización de escritorios de IsardVDI. 

La idea de este documento es aportar y recoger ideas sobre qué elementos del windows modificar y personalizar para un mejor rendimiento.

## Pre-instalación windows 10

Esta primera parte explica donde se tiene que buscar las imágenes iso, los drivers de virtio y como configurar el hardware del escritorio virtual.

### ISO de windows

En función del tipo de licencia y del idioma podemos descargar una iso genérica de windows 10. En nuestro  caso queremos licenciar el windows con una licencia Pro. Las versiones de las iso de windows van evolucionando, y desde la página web de microsoft esta disponible solo la última. También existe la posibilidad de personalizar tu propia iso de windows o crear versiones LTSC (en este caso la licencia es enterprise) con herramientas de microsoft.

Se va a la [página web de microsoft](https://www.microsoft.com/es-es/software-download/windows10ISO) para descargar la iso y se selecciona la edición y el idioma

![](./win10_install_guide.es.images/win17.png)

La página permite descargar la versión de 64 bits. En cuanto se empiece a descargar la iso, se cancela la descarga, ya que sólo interesa el enlace de la descarga para que la descargue dentro de los directorios de isard donde se almacenan las iso. Se copia el enlace de descarga, este enlace es válido solo temporalmente.

![](./win10_install_guide.es.images/win18.png)

Se pulsa el botón ![](./win10_install_guide.es.images/win1.png) para ir al panel de administración

![](./win10_install_guide.es.images/win2.png)

En el apartado de ![](./win10_install_guide.es.images/win4.png)

![](./win10_install_guide.es.images/win3.png)

Se pulsa el botón ![](./win10_install_guide.es.images/win5.png)

![](./win10_install_guide.es.images/win6.png)

Aparece un cuadro de diálogo donde se puede copiar el enlace de descarga que previamente se ha copiado, se le da un nombre y se selecciona el tipo ISO. También se puede aprovechar para dar permisos para que esta iso la puedan usar los usuarios desplegando la zona de "Allows":

![](./win10_install_guide.es.images/win7.png)

La imagen iso se queda en Status "Downloading" y se puede ir viendo el progreso de la descarga:

![](./win10_install_guide.es.images/win8.png)

### Windows VirtIO Drivers

Mientras se descarga, se puede ir a buscar la iso con los drivers virtio que necesitaremos. Los drivers virtio permiten usar dispositivos paravirtualizados o simulados que forman parte del hardware optimizado para mejorar el rendimiento de las máquinas virtuales de KVM. Destacan los drivers virtio de disco, más eficientes que los simuladores de SATA o IDE, así como los drivers de red virtio, más eficiente que las simulaciones de otras tarjetas. 

La comunidad de Fedora, con su proyecto oVirt como referencia, preparan y mantienen una imagen ISO con un par de instaladores de windows que facilitan la instalación de software necesario. Sería algo parecido a los discos de drivers de las placas base, que vienen con un instalador para facilitarte la instalación manual de todos los drivers disponibles. Hay dos versiones disponibles:

* [Stable](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso)
* [Latest](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso)

Se recomienda usar la versión latest y en caso de dar algún problema, ir a la versión stable. 

Se descarga también esta imagen iso en isard, siguiendo los mismos pasos que se han realizado anteriormente para la iso de windows.

![](./win10_install_guide.es.images/win9.png)

## Crear escritorio para instalar desde iso

Para crear un escritorio desde un "Media", se pulsa el botón ![](./win10_install_guide.es.images/win10.png) desde la iso:

![](./win10_install_guide.es.images/win11.png)

Se selecciona como plantilla de sistema operativo (la que decide que hardware es el aconsejable para esta distribución) "windows 10 UEFI and Virtio devices". Se le da un nombre al escritorio, y se define sus capacidades: 2vcpus, 6GB y disco de 120GB por ejemplo... 

![](./win10_install_guide.es.images/win12.png)

Una vez se ha pulsado en "Create desktop", vamos al menú Desktops a ver si ya se ha creado. Observamos que solo tiene un icono de media ![](./win10_install_guide.es.images/win13.png)

![](./win10_install_guide.es.images/win19.png)

Para desplegar las opciones, se pulsa el icono ![](./win10_install_guide.es.images/win14.png), y se pulsa el botón ![](./win10_install_guide.es.images/win15.png)

Aparece un cuadro de diálogo donde se puede desplegar Media con el icono ![](./win10_install_guide.es.images/win16.png)

En la parte de CD/DVD, se busca la palabra "virt" y se selecciona la iso de virtio-win

![](./win10_install_guide.es.images/win20.png)

Quedan las dos iso seleccionadas en Media 

![](./win10_install_guide.es.images/win21.png)

Se verifica que la opción de boot está en cd/dvd

Ahora cambiamos el tipo de red de **Default** a **Intel PRO/1000 isolated**. Este tipo de tarjeta de red Intel Pro, conectada a una red isolated (puede salir a internet pero no puede ver a otros escritorios) permitirá que el instalador de windows reconozca esa tarjeta y pueda salir a internet durante la instalación. En caso de que dejemos la red default la instalación se realiza igualmente pero no realiza ciertas actualizaciones ni ofrece ciertas opciones durante el proceso de instalación. Recomendamos cambiar a este tipo de hardware y volver a modificar posteriormente a la red **"Default"** una vez haya finalizado la instalación, ya que con la red default se utiliza un hardware "**virtio**" que es más eficiente y da más rendimiento que la simulación de tarjeta Intel Pro 1000. 

![](./win10_install_guide.es.images/win22.png)

![](./win10_install_guide.es.images/win24.png)

Se pulsa el botón ![](./win10_install_guide.es.images/win23.png) y si todo ha ido bien aparece el icono de Start y dos iconos de media en el listado:

![](./win10_install_guide.es.images/win25.png)

Internamente el escritorio virtual corre en los hypervisores a partir de un xml que describe esa máquina virtual. 
Desde el panel de administración se puede acceder a este fichero xml y verificar que ciertas opciones están seleccionadas. Primero se pulsa el botón ![](./win10_install_guide.es.images/win26.png) dentro del apartado de "Domains"

![](./win10_install_guide.es.images/win27.png)

Se despliega las opciones del desktop y se pulsa el botón ![](./win10_install_guide.es.images/win28.png). 

![](./win10_install_guide.es.images/win29.png)

![](./win10_install_guide.es.images/win30.png)

**Se recomienda solo visualizar y tener un conocimiento claro de lo que se quiere hacer si tocamos este xml.**

Importantes las siguientes secciones:

* Tipo de máquina **q35** , es un tipo de simulación de hardware moderna y con mejor rendimiento para el windows 10

```xml
    <type arch="x86_64" machine="q35">hvm</type> 
```

* opciones especiales para simular y gestionar interrupciones y interacción simulando al hypervisor de Microsoft HyperV, mejorando el rendimiento general de la máquinas de windows. Con el tipo de máquina q35 son necesarias estas opciones

```xml
      <hyperv>
        <relaxed state="on"/>
        <vapic state="on"/>
        <spinlocks state="on" retries="8191"/>
      </hyperv>
```
  
* Arranque UEFI. Necesario para una correcta instalación de windows 10 y detección de los drivers virtio de disco durante el inicio de la instalación. Se usa para simular UEFI el loader de UEFI, y hay que indicarle en que path tiene ese código de arranque del UEFI. La línea del xml que activa el UEFI es:

```xml
        <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
```

Integrado en la zona de definición de os en el xml:

```xml
      <os>
        <type arch="x86_64" machine="q35">hvm</type>
        <boot dev="cdrom"/>
        <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
      </os>
```


La primera vez que se arranca con uefi, como no hay nada instalado y el menú de arranque por cd casi no tiene timeout, no hay sistema operativo y se queda en este punto:

![](./win10_install_guide.es.images/win31.png)

Se fuerza el reinicio enviando la combinación CTRL+ALT+SUPR. Esta parte de enviar esta combinación se puede hacer dentro del visor remote-viewer en el menú "Send key":

![](./win10_install_guide.es.images/win32.png)

Se puede ver el mensaje que nos pide arrancar por cd. Dentro del remote-viewer, pulsar una tecla

![](./win10_install_guide.es.images/win33.png)

Aparece el icono en pantalla del tiano core:

![](./win10_install_guide.es.images/win34.png)

Y la primera pantalla de instalación del windows:

![](./win10_install_guide.es.images/win35.png)


## Instalación de windows

Se pulsa "siguiente" de esta primera pantalla de sección de idiomas:

![](./win10_install_guide.es.images/win36.png)

Instalar ahora:

![](./win10_install_guide.es.images/win37.png)

En el siguiente diálogo se pulsa **"No tengo clave de producto"**

![](./win10_install_guide.es.images/win38.png)

Se selecciona el tipo de windows que se quiere instalar, en nuestro caso el **Windows 10 Pro**

![](./win10_install_guide.es.images/win39.png)

Se acepta los términos de licencia:

![](./win10_install_guide.es.images/win40.png)

En el tipo de instalación se selecciona la opción personalizada:

![](./win10_install_guide.es.images/win41.png)

Se carga el controlador de virtio en el botón **"Cargar contr."**:

![](./win10_install_guide.es.images/win42.png)

Se pulsa examinar y se selecciona el cdrom de virtio, directorio viostor, directorio w10 y amd64:

![](./win10_install_guide.es.images/win43.png)

Se selecciona el controlador:

![](./win10_install_guide.es.images/win44.png)

Y se detecta el disco para que se pueda instalar:

![](./win10_install_guide.es.images/win45.png)

Se pulsa "Siguiente" y se inicia la instalación del sistema operativo:

![](./win10_install_guide.es.images/win46.png)

Una vez que acaba, se reinicia el escritorio virtual:

![](./win10_install_guide.es.images/win47.png)

Intenta arrancar por cdrom y luego UEFI da un error:

![](./win10_install_guide.es.images/win48.png)

Ahora desde la aplicación web de Isard se detiene la máquina y se cambia el dispositivo de arranque. Se pulsa el botón ![](./win10_install_guide.es.images/win49.png)

![](./win10_install_guide.es.images/win50.png)

Se despliega las opciones y se pulsa el botón de **Edit** 

![](./win10_install_guide.es.images/win51.png)

Se cambia el dispositivo de **Boot** a **Hard Disk** y se modifica el escritorio

![](./win10_install_guide.es.images/win52.png)

Se vuelve a arrancar el escritorio, se pulsa el botón "Start":

![](./win10_install_guide.es.images/win53.png)

Me conecto al escritorio y arranca bien desde disco, aparecen mensajes por pantalla como este:

![](./win10_install_guide.es.images/win54.png)

Se acaba con un nuevo reinicio y aparece un conjunto de diálogos donde se selecciona habitualmente NO a lo que propone:

![](./win10_install_guide.es.images/win55.png)

![](./win10_install_guide.es.images/win56.png)

Si se ha seleccionado la red con modelo de tarjeta Intel, aparecerá una pantalla como esta que busca actualizaciones por internet:

![](./win10_install_guide.es.images/win57.png)

Si no se selecciona la tarjeta intel, como no tiene los drivers de red no puede acceder a internet, se le dice que no tenemos internet cuando proponga conectarnos a una red:

![](./win10_install_guide.es.images/win58.png)

![](./win10_install_guide.es.images/win59.png)

y en el siguiente diálogo se selecciona: **Continuar con la configuración limitada**

A continuación, si se tenía red, permite realizar la configuración para uso personal o para una organización. En nuestro caso se ha configurado para uso personal:

![](./win10_install_guide.es.images/win60.png)

Y ahora para añadir una cuenta se selecciona **Cuenta sin conexión**

![](./win10_install_guide.es.images/win61.png)

Y posteriormente se selecciona **Experiencia limitada**:

![](./win10_install_guide.es.images/win62.png)

Al ser una imagen genérica se tiene que poner un usuario y contraseña que sea fácil de recordar. En Isard es habitual que las imágenes base vengan con el siguiente usuario y contraseña que configuraremos

* usuario: isard
* contraseña: pirineus

![](./win10_install_guide.es.images/win63.png)

![](./win10_install_guide.es.images/win64.png)

Se rellena las preguntas de seguridad con respuestas aleatorias:

![](./win10_install_guide.es.images/win65.png)

Ahora se opta por decirle que no a lo que se vaya proponiendo:

![](./win10_install_guide.es.images/win66.png)

![](./win10_install_guide.es.images/win67.png)

![](./win10_install_guide.es.images/win68.png)

![](./win10_install_guide.es.images/win69.png)

![](./win10_install_guide.es.images/win70.png)

![](./win10_install_guide.es.images/win71.png)

![](./win10_install_guide.es.images/win72.png)

No se quiere el asistente Cortana:

![](./win10_install_guide.es.images/win73.png)

![](./win10_install_guide.es.images/win74.png)

Finalmente, empieza a dar mensajes de bienvenida:

![](./win10_install_guide.es.images/win75.png)

Y aparece el escritorio de windows por primera vez:

![](./win10_install_guide.es.images/win76.png)

Ahora ya se tiene el windows 10 con un escritorio de trabajo. El siguiente paso es instalar los drivers. Para eso es importante cambiar el tipo de tarjeta de red a virtio, ya que ese hardware está optimizado y es el recomendado al trabajar con una máquina virtual que corre sobre KVM como es nuestro caso. Por lo tanto se apaga, se modifica el hardware y se vuelve a arrancar el escritorio. 

![](./win10_install_guide.es.images/win77.png)

Ahora se edita el escritorio desplegando las opciones y se pulsa el botón **Edit** y se selecciona la red **Default** para que detecte los drivers virtio de red:

![](./win10_install_guide.es.images/win78.png)

![](./win10_install_guide.es.images/win79.png)

Se vuelve a iniciar el escritorio y se conecta por spice con remote-viewer:

Se abre el explorador de ficheros, se selecciona la unidad E: y se arranca primero el ejecutable virtio-win-gt-x64 y luego virtio-guest-tools:

![](./win10_install_guide.es.images/win80.png)

Instalador de drivers para windows Virtio-win:

![](./win10_install_guide.es.images/win81.png)

Se pulsa el botón de "Siguiente":

![](./win10_install_guide.es.images/win82.png)

Se pulsa el botón de "Si" a que se quiere permitir que instale:

![](./win10_install_guide.es.images/win83.png)

Se confía en que el editor es Red Hat y "Instalar":

![](./win10_install_guide.es.images/win84.png)

Ahora las win-guest-tools que permitirán entre otras cosas, el reescalamiento automático de la resolución de pantalla.

![](./win10_install_guide.es.images/win85.png)

Al instalar el agente de spice ya reescala automáticamente. 

Se hace un apagado manual y se quita los cds del isard con el escritorio apagado:

![](./win10_install_guide.es.images/win77.png)

Para eliminar las isos, se pulsa el icono "X" de las dos:

![](./win10_install_guide.es.images/win86.png)

Quedando en el listado del escritorio sin iconos en la columna "Media":

![](./win10_install_guide.es.images/win87.png)

Se vuelve a arrancar el escritorio y a conectarnos con el visor remote-viewer. 

A continuación se instala actualizaciones, se escribe **"windows update"** en el buscador de la barra de tareas:

![](./win10_install_guide.es.images/win88.png)

Se pulsa **Buscar actualizaciones**:

![](./win10_install_guide.es.images/win89.png)

Encuentra algunas actualizaciones y se dispone a instalarlas

![](./win10_install_guide.es.images/win90.png)

Se realiza un reboot al escritorio desde dentro del propio windows, tarda en reiniciar al aplicar las actualizaciones:

![](./win10_install_guide.es.images/win91.png)  ![](./win10_install_guide.es.images/win92.png)

### Crear plantilla de windows actualizado y con drivers virtio instalados.

Una vez se tiene el sistema actualizado y se ha realizado un reinicio para verificar que las actualizaciones han quedado bien instaladas, se recomienda hacer una plantilla para guardar todo el trabajo hecho y poder regresar a este punto de la instalación si lo necesitamos en un futuro. Tener una plantilla con lo realizado hasta el momento puede ahorrar mucho tiempo para confeccionar una nueva plantilla con optimizaciones y software en un futuro.

Las plantillas sólo se pueden hacer con el escritorio apagado. Por lo que lo primero es apagar de forma ordenada el windows:

![](./win10_install_guide.es.images/win93.png)

Ahora desde desktops, con las opciones ampliadas desplegadas en nuestro escritorio:

Una vez se haya reiniciado, se apaga de forma ordenada y se crea la plantilla. En la fila del escritorio con la máquina apagada se pulsa al botón "**Template it**":

![](./win10_install_guide.es.images/win94.png)

Y se crea la plantilla

![](./win10_install_guide.es.images/win95.png)

A partir de ahora, la plantilla aparecerá en el listado de templates, y nuestro escritorio derivará de esta plantilla que acabamos de crear. Podemos continuar trabajando con el escritorio en el punto donde se ha dejado y también crear nuevos escritrios a partir de esa plantilla.


## Software y optimizaciones de Windows

### Modificar el idioma y replicar a todos los usuarios

En caso de que se quiera modificar el idioma de windows (por ejemplo a catalán) es importante realizar el cambio ahora ya que cierto software buscará el idioma del usuario para instalar la versión correcta de idioma. 

Se va a **configuración => hora e idioma => idioma**

Ahora se añade un idioma:

![](./win10_install_guide.es.images/win96.png)

![](./win10_install_guide.es.images/win97.png)

Se selecciona **"Establecer como idioma para mostrar de windows"** y se pulsa el botón "Instalar":

![](./win10_install_guide.es.images/win98.png)

Tarda unos minutos en aparecer la barra de descarga:

![](./win10_install_guide.es.images/win99.png)

Pide si se quiere cerrar sesión ahora, se acepta, se cierra sesión y se vuelve a iniciar sesión, aparece el catalán en la barra de tareas de windows:

![](./win10_install_guide.es.images/win100.png)

#### Configurar idioma por defecto para todos los usuarios

Se va a **Inicio -> configuración -> Hora e idioma -> Idioma**

Ahora se puede quitar el idioma Español si se desea o bien dejar los dos idiomas en el usuario isard. 

Debajo de idiomas preferidos aparece la opción de **Configuración de idioma administrativo**

![](./win10_install_guide.es.images/win101.png)

Se abre un cuadro de diálogo:

![](./win10_install_guide.es.images/win102.png)

Se pulsa en cambiar los parámetros regionales del sistema y se cambia a "Català"

Y ahora la parte más importante para que los nuevos usuarios también tengan el nuevo idioma por defecto. Se pulsa en **Copia la configuración** y se selecciona **Pantalla de bienvenida y cuentas del sistema** y **Cuentas de usuarios nuevos**. Queda una configuración como esta:

![](./win10_install_guide.es.images/win103.png)

Se aplica los cambios y se reinicia:

![](./win10_install_guide.es.images/win104.png)

Aun pueden quedar restos del idioma original (español en este caso) en el usuario actual, se tendría que borrar la cuenta de usuario y volver a crearla para que al rehacer de nuevo la cuenta coja bien todas las opciones de idioma. Por eso es importante hacer el cambio de idioma antes de crear los usuarios admin y user que son los que se darán por defecto en la plantilla base de windows.


## Adaptar windows para entorno virtualizado

En un entorno virtualizado nos interesa que los recursos que usa el windows sean mínimos a nivel de escritura en disco, uso de cpu y ram. Cuanto menor impacto en estos factores tengamos, más escritorios simultáneos se puede correr en un servidor. Una disminución de procesos que habitualmente el usuario no usa en un entorno educativo o empresarial también favorecerá un inicio del sistema más rápido y mayor agilidad. El reto está en buscar el equilibrio entre restringir funcionalidades y no penalizar la experiencia de usuario. 

En esta guía se proponen diferentes herramientas y técnicas que permitirán optimizar las instalaciones y mejorar la experiencia de usuario.

### Instalar firefox

Se descarga el firefox y se instala desde la [web de mozilla](https://www.mozilla.org/es-ES/firefox/new/):

Se inicia por primera vez el firefox. Se apaga, y se vuelve a arrancar. Nos pregunta:

![](./win10_install_guide.es.images/win105.png)

Le decimos que sí se quiere y se cambia el navegador predeterminado:

![](./win10_install_guide.es.images/win106.png)

Y queda predeterminado:

![](./win10_install_guide.es.images/win107.png)

### Desinstalar Microsoft Edge

Microsoft Edge está basado en Chromium project, se puede dejar en el sistema, pero si se desea quitar del sistema operativo se puede seguir los siguientes pasos. Se abre una consola: se pulsan las teclas **Windows + R** y se escribe **cmd**

En la consola:

```bat
cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
setup --uninstall --force-uninstall --system-level
```

### Autologon

Para facilitar los reinicios y de momento como se trabajará con un solo usuario administrador para realizar instalaciones y preparar la plantilla se propone instalar una utilidad para realizar un autologon y que no nos pida usuario y password cuando inicia windows. Se tiene que buscar "autologon windows sysinternals" y llegar a la web de sysinternals desde donde se descarga la utilidad. El enlace donde encontrar la utilidad es: [Autologon](https://docs.microsoft.com/en-us/sysinternals/downloads/autologon)

![](./win10_install_guide.es.images/win108.png)

A continuación se descarga el fichero zip que nos proporcionan, se abre y se descomprime **Autologon64**:

![](./win10_install_guide.es.images/win109.png) 

![](./win10_install_guide.es.images/win110.png)

Se ejecuta Autologon64, se introduce la contraseña de la cuenta isard: **pirineus**

![](./win10_install_guide.es.images/win111.png)

Nos informa que la contraseña queda encriptado:

![](./win10_install_guide.es.images/win112.png)

Se reinicia para verificar que ya no nos pide usuario y password al entrar:

![](./win10_install_guide.es.images/win113.png)

### Crear usuario admin

Una cuenta admin nos permitirá poder trabajar como administradores en caso de que se quiera convertir la cuenta del usuario "isard" a usuario normal.

Desde powershell como administrador (botón derecho encima del icono de windows):

![](./win10_install_guide.es.images/win114.png)

```
# Guardar el password en una variable
$Password = Read-Host -AsSecureString
```
Y una vez se ha introducido el password: ***pirineus***

```powershell
# Crear usuario admin del grupo administradores
New-LocalUser "admin" -Password $Password -FullName "admin"
Add-LocalGroupMember -Group "Administradores" -Member "admin"
```

Y el usuario "user" que no será administrador:

```
New-LocalUser "user" -Password $Password -FullName "user"
Add-LocalGroupMember -Group "Usuarios" -Member "user"
```


### Directorio c:\admin

También se crea el directorio c:\admin donde se guardarán los programas o información de administración. Se quitan los permisos de usuarios, para que solo puedan acceder administradores:

![](./win10_install_guide.es.images/win115.png)

Y se deja solo el permiso para el grupo administradores:

![](./win10_install_guide.es.images/win116.png)


### Desinstalar Microsoft Edge

Microsoft Edge está basado en Chromium project, se puede dejar en el sistema, pero si se desea quitar del sistema operativo se puede seguir los siguientes pasos. Se abre una consola: Se pulsan las teclas **Windows + R** y se escribe **cmd**

En la consola:

```bat
cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
setup --uninstall --force-uninstall --system-level
```

### Desinstalar otras aplicaciones de Microsoft

Se abre una consola de powershell como administrador:

- Botón derecho encima del icono de windows en la barra de inicio y se selecciona **Windows PowerShell (Administrador)**

![](./win10_install_guide.es.images/win117.png)

Para mirar todos los programas instalados:

```
Get-AppxPackage | Select Name
```

Y ahora se borra todos estos:

```
Microsoft.Xbox*
Microsoft.Bing*
Microsoft.3DBuilder
Microsoft.Advertising.Xaml
Microsoft.AsyncTextService
Microsoft.BingWeather
Microsoft.BioEnrollment
Microsoft.DesktopAppInstaller
Microsoft.GetHelp
Microsoft.Getstarted
Microsoft.Microsoft3DViewer
Microsoft.MicrosoftEdge
Microsoft.MicrosoftEdgeDevToolsClient
Microsoft.MicrosoftOfficeHub
Microsoft.MicrosoftSolitaireCollection
Microsoft.MicrosoftStickyNotes
Microsoft.MixedReality.Portal
Microsoft.Office.OneNote
Microsoft.People
Microsoft.ScreenSketch
Microsoft.Services.Store.Engagement
Microsoft.Services.Store.Engagement
Microsoft.SkypeApp
Microsoft.StorePurchaseApp
Microsoft.Wallet
Microsoft.WindowsAlarms
Microsoft.WindowsCamera
Microsoft.WindowsFeedbackHub
Microsoft.WindowsMaps
Microsoft.WindowsSoundRecorder
Microsoft.WindowsStore
Microsoft.XboxGameCallableUI
Microsoft.YourPhone
Microsoft.ZuneMusic
Microsoft.ZuneVideo
SpotifyAB.SpotifyMusic
Windows.CBSPreview
microsoft.windowscommunicationsapps
windows.immersivecontrolpanel
```

Ahora se copia y se pega en la terminal de powershell

```
Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
```

### Barra de programas simple

Se puede quitar todos los elementos de Productividad y Explorar de la barra de inicio.

Se pulsa el botón derecho encima de los elementos y **Desanclar de Inicio**

![](./win10_install_guide.es.images/win118.png)


### Desactivar servicios

Se ejecuta "**msconfig**" y se desactiva servicios:

- Administrador de autenticación Xbox Live
- Centro de seguridad
- Firewall de Windows Defender
- Mozilla Maintenance Service
- Servicio de antivirus de Microsoft Defender
- Servicio de Windows Update Medic
- Windows Update

En Inicio de windows -> ir al administrador de tareas y deshabilitar:

- Microsoft OneDrive

- Windows Security notification

  ![](./win10_install_guide.es.images/win119.png)
  
  Se reiniciar para aplicar los cambios
  
  ![](./win10_install_guide.es.images/win120.png)
  

### Repasar escritorios de admin y user

Se inicia sesión y se repasa que los iconos de escritorio, iconos de barra de tareas y menú inicio están bien... Volver a quitar los programas de windows...

Para quitar las notificaciones:

![](./win10_install_guide.es.images/win121.png)


### Instalar software libre

Se elige instalar los siguientes software:

- Libre Office

- Gimp

- Inkscape

- LibreCAD

- Geany

![](./win10_install_guide.es.images/win122.png)

### Aplicaciones no libres

Se instalan las siguientes aplicaciones:

- Google chrome
- Adobe Acrobat Reader

Se desactiva Update Service:

![](./win10_install_guide.es.images/win123.png)

Se desactiva Google Update:

![](./win10_install_guide.es.images/win124.png)

Una herramienta muy útil para preparar plantillas es **VMware OS Optimization Tool** 

Se puede descargar desde: [VMware OS Optimization tool](https://flings.vmware.com/vmware-os-optimization-tool)

Se deja en c:\admin\VMwareOSOptimizationTool_b2000_17205301.zip

Antes de pasar esta herramienta se recomienda hacer una plantilla. Si la herramienta de optimización realiza demasiadas modificaciones que interfieren con alguna de las aplicaciones que se ha instalada se puede volver a este punto y realizar la optimización posteriormente.

## Vmware OS optimization tool

Se descomprime el fichero zip que se ha almacenado en c:\admin y se arranca la utilidad: VMwareOSOptimizationTool

![](./win10_install_guide.es.images/win125.png)

Una vez arranca la aplicación se pulsa el botón **Analyze**:

![](./win10_install_guide.es.images/win126.png)

Y después a "Common options", donde en Visual Effect se selecciona "Best quality" y atención al check de "Disable hardware acceleration":

![](./win10_install_guide.es.images/win127.png)

En Store Apps se deja "Calculator"

![](./win10_install_guide.es.images/win128.png)

Y se pulsa el botón de "Optimize", cuando finaliza se guarda el resultado con el botón "export" en c:\admin y luego se reinicia.
