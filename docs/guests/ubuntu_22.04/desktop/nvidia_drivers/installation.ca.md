# GPU - Drivers NVIDIA - Ubuntu Desktop

## Instal·lació

A partir d'un [escriptori Ubuntu Desktop 22.04 LTS](../installation/installation.ca.md), s'agafa la última versió dels drivers NVIDIA al [repositori de Google, per sistemes operatius basats en Linux](https://cloud.google.com/compute/docs/gpus/grid-drivers-table?hl=es-419#linux_drivers).

S'ha d'[assignar un perfil virtual de GPU a l'escriptori](../../../../user/edit_desktop.ca.md/#reservables) abans d'[iniciar-ho](../../../../user/bookings.ca.md/#reservar-un-escriptoridesplegament).

A la terminal s'executa:
```
$ sudo apt install build-essential
$ sudo bash NVIDIA-Linux-x86_64-525.105.17-grid.run
```

I es segueixen els passos següents:

![](images/1.png)

![](images/2.png)

![](images/3.png)

![](images/4.png)

![](images/5.png)

Es reinicia l'escriptori.


## Llicenciar client

Es fa mitjançant el token, un arxiu que es genera un cop configurat un servidor CLS de llicències a la [plataforma de llicenciament NVIDIA](https://ui.licensing.nvidia.com).

Es mou el token a l'escriptori tal cual es descarrega, **no val copiar el contingut i crear un nou fitxer a l'escriptori** donat que hi vénen subdades a aquest fitxer que no es poden regenerar copiant i enganxant el codi d'encriptació de dins l'arxiu (és un arxiu format *.tok*). Es pot fer mitjançant el visor SPICE, arrossegant des de l'ordinador host fins al visor de l'Ubuntu.

S'ha de moure el token al directori **/etc/nvidia/ClientConfigToken**, i es canvien els permisos:

```
$ sudo mv client_configuration_token_*.tok /etc/nvidia/ClientConfigToken
$ sudo chmod 744 /etc/nvidia/ClientConfigToken/client_configuration_token_*.tok
```

Si no existeix l'arxiu **gridd.conf** es crea a partir de l'arxiu **gridd.conf.template** (no s'ha de moficiar res de l'arxiu):

```
$ sudo cp gridd.conf.template gridd.conf
```

S'apaga l'escriptori i es modifica per tal de [canviar el Video perquè funcioni amb GPU](../../../../user/bookings.ca.md/#preparar-escriptoridesplegament)

S'incia i a la terminal s'escriu el comandament 
```
$ nvidia-smi -q
```

On s'ha de desplaçar fins a trobar el següent apartat, on si **License Status** surt **Licensed** amb una data de venciment, confirma la correcta llicència del client Ubuntu.
```
vGPU Software Licensed Product
        Product Name                      : NVIDIA RTX Virtual Workstation
        License Status                    : Licensed (Expiry: 2023-5-25 12:53:35)
```
