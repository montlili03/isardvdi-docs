# Despliegues

!!! Info
    Esta sección es solo para **administradores**, **gestores** y usuarios **avanzados**

Los despliegues son una herramienta potente que permite a los usuarios avanzados crear un conjunto de escritorios basados en una única plantilla. Todos los usuarios incluidos en un despliegue tendrán acceso a su propio escritorio único.

Con los despliegues, se pueden crear y gestionar varios escritorios simultáneamente para todos los usuarios seleccionados. Como creador, se puede gestionar fácilmente todos los escritorios en un solo lugar a través de la sección Despliegues. Incluso existe la posibilidad de interactuar con cada escritorio y ver todas las pantallas simultáneamente.

## Como crear despliegues

Para crear un despliegue, se ha de ir a la barra superior y pulsar el botón ![Botón superior de despliegues](./deployments.es.images/deployments1.png)

Redirigirá a esta página:

![página de despliegues](./deployments.es.images/deployments4.png)

Se pulsa el botón ![Nuevo despliegue](./deployments.es.images/deployments3.png)

Se dirigirá a un formulario donde introducir la información necesaria para el nuevo despliegue, incluyendo el nombre del despliegue, cada nombre de escritorio que verán los usuarios y una breve descripción.

![formulario de despliegue nuevo](./deployments.es.images/deployments6.png)

Además, se ha de seleccionar una plantilla desde donde crear vuestros escritorios. Una vez creada, vuestros escritorios se harán visibles para los usuarios, hacer clic en ![no visible / visible](./deployments.es.images/deployments5.png)

También se debe especificar los usuarios que se tienen que incluir en el despliegue. En la sección final del formulario, se puede seleccionar el grupo necesario de usuarios, así como cualquier usuario adicional.

!!! Note "Nota"
    También se creará un escritorio para vuestro usuario dentro del despliegue

La última sección son las opciones avanzadas. Aquí podéis cambiar los visores de los escritorios, el hardware y la imagen de la miniatura.

## Inicio y parada

Para acceder al despliegue, hacer clic en el nombre del despliegue.

![lista de despliegues](./deployments.es.images/deployments7.png)

Para iniciar un escritorio, pulsar el botón ![Iniciar](./deployments.ca.images/deployments8.png)

Una vez iniciado, detectará la "IP" del escritorio (si tiene), el "Estado" (si está iniciado o parado), y el tipo de visor se puede seleccionar pulsando el menú desplegable:

![lista desplegable de los visores](./deployments.es.images/deployments10.png)

Para parar un escritorio iniciado, simplemente hacer clic en el botón ![parar](./deployments.es.images/deployments12.png) que aparecerá. Esto cambiará su estado a "Parando". Si bien podéis utilizar la opción "Forzar parada" para pararlo inmediatamente, ésto puede provocar problemas de cierre.

Para parar todos los escritorios a la vez, pulsar el botón ![icono de parada](./deployments.ca.images/deployments16.png)

## Videowall

Videowall es una función que permite ver todas las pantallas de los escritorios de un despliegue particular simultáneamente y en tiempo real. También permite interactuar con cualquiera de los escritorios mostrados.

!!! Warning "Cuidado"
    Entrar dentro de la pantalla de otro usuario tomará el control del usuario propietario.

!!! Danger "Aviso legal"
    Se requiere el permiso del propietario antes de acceder a su escritorio. El acceso no autorizado puede tener consecuencias legales.

Para acceder al Videowall, hacer clic al botón ![icono del videowall](./deployments.ca.images/deployments18.png)

Se redirigirá a una página donde poder ver los escritorios de todos los usuarios.

![pantalla del videowall](./deployments.es.images/deployments16.png)

Para interactuar con un escritorio, hacer clic al botón ![maximiza](./deployments.ca.images/deployments21.png) que redirigirá a una vista a pantalla completa del escritorio seleccionado.

![pantalla completa del videowall](./deployments.es.images/deployments17.png)

!!! Warning "Visores RDP"
    En Videowall no se mostraran escritorios que utilicen visores RDP.

## Hacer visible/invisible

Al hacer visible o invisible el despliegue, se determina si los usuarios que tienen acceso al mismo podrán ver y acceder a su escritorio, o no.

### Visible

Para hacerlo visible, hacer clic en el botón ![botón de ojo tachado](./deployments.ca.images/deployments23.png)

Aparecerá una ventana de diálogo

![hacer visible](./deployments.es.images/deployments20.png)

Una vez "Visible" es seleccionado, el despliegue se volverá de color verde

![despliegue verde](./deployments.es.images/deployments21.png)

### Invisible

Para hacerlo invisible, hacer clic en el botón ![botón ojo abierto](./deployments.ca.images/deployments26.png)

Aparecerá una ventana de diálogo

![hacer invisible](./deployments.es.images/deployments23.png)

Hay dos opciones para elegir:

- **No parar escritorios**: Esta opción hará que el despliegue sea invisible para el usuario. Sin embargo, si un usuario está utilizando actualmente su escritorio en el momento en que se hace invisible, todavía podrán utilizarlo hasta que se decida pararlo. Una vez parado, el usuario no podrá acceder de nuevo hasta que el despliegue se vuelva a hacer visible.

- **Parar escritorios**: Esta opción también hará invisible el despliegue. Sin embargo, si cualquier usuario ya está utilizando su escritorio, se apagará inmediatamente.

Una vez seleccionado "Invisible", el despliegue se volverá de color rojo

![despliegue rojo](./deployments.es.images/deployments18.png)

## Recrear despliegue

La función "Recrear despliegue" tiene como objetivo realizar cambios en el despliegue mediante la adición o eliminación de escritorios, en base a las modificaciones realizadas en los permisos del usuario. En caso de que se agregue o elimine un usuario, esta función actualizará el despliegue incorporando o eliminando los escritorios correspondientes.

Para hacer esto, hacer clic en el botón ![icono recrear](./deployments.ca.images/deployments31.png)

![notificación](./deployments.es.images/deployments25.png)

## Descargar fichero de visores directos

En este archivo se muestra el listado de usuarios incluidos en el despliegue, así como las URLs correspondientes a los visores de sus respectivos escritorios.:

Para lograr esto, se ha de hacer clic en el botón ![icono de descarga](./deployments.ca.images/deployments34.png)

Aparecerá una notificación:

![notificación](./deployments.es.images/deployments27.png)

Confirmar la notificación para descargar el fichero CSV.

Este fichero contiene cada nombre de usuario, su nombre, su correo electrónico y un enlace que redirige al escritorio del usuario.

![contenido del fichero csv](./deployments.ca.images/deployments36.png)

## Borrar un despliegue

Para borrar un despliegue, pulsar el botón ![icono de la papelera](./deployments.ca.images/deployments37.png) en la lista de despliegues. También se eliminarán todos los escritorios que lo conforman.
